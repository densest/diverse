import networkx as nx
from gurobipy import GRB, Model, tupledict
import gurobipy as gp

from utils import readFile, dataset_path

TIMELIMIT = 60 * 60 * 36
LOGFILE = 'gurobilog.txt'


def run_ilp(G, h, k):
    m = Model("atleasthedgesilp")

    m.setParam('TimeLimit', TIMELIMIT)

    x = tupledict()
    y = tupledict()

    for u in G.nodes():
        y[u] = m.addVar(vtype=GRB.BINARY, name='y[%d]' % u)

    for u, v in G.edges():
        x[u, v] = m.addVar(vtype=GRB.BINARY, name='x[%d,%d]' % (u, v))

        m.addConstr(x[u, v] <= y[u])
        m.addConstr(x[u, v] <= y[v])

    m.addConstr(gp.quicksum(x[u, v] for u, v in G.edges()) >= h)

    m.addConstr(gp.quicksum(y[u] for u in G.nodes()) == k)

    m.setObjective((gp.quicksum(x[u, v] for u, v in G.edges())) / k, GRB.MAXIMIZE)

    m._vars = vars
    m.optimize()

    if m.status != GRB.OPTIMAL:
        print("infeasible")
        return {}

    vals = m.getAttr('x', x)

    return vals


def run_exact(G, k, h):
    result = run_ilp(G, k=k, h=h)
    num_edges = 0
    nodes = set()
    for r in result:
        assignment = result[r]
        if assignment == 1.0:
            # print(r, result[r])
            num_edges += assignment
            nodes.add(r[0])
            nodes.add(r[1])

    print(len(nodes), num_edges, k, num_edges / k)
    # print(nodes)
    return num_edges / k


def load_graph(filename):
    data = readFile(filename)
    g = nx.Graph()
    for d in data:
        s = d.split(" ")
        u = int(s[0])
        v = int(s[1])
        g.add_edge(u, v)
    return g



def run(g, start, dsname, stepsize=1):

    h = start
    all_best_results = []

    while h <= g.number_of_edges():
        lower = 1 #round(0.5 * (1 + math.sqrt(1 + 8 * h))) - 1
        upper = g.number_of_nodes() + 1
        results = {}


        for k in range(lower, upper):
            print("running ilp with k ", k, upper)
            r = run_exact(g.copy(), k, h)
            results[k] = r

        print(results)
        best_result = max(results.items(), key=lambda k: k[1])
        print(best_result)
        all_best_results.append(best_result)

        h += 1

    print(all_best_results)

    with open("results_" + dsname + "_atleasth_exact.txt", "w") as output:
        output.write(str(all_best_results))



if __name__ == '__main__':
    datasets = [
         ("aucs", 281),
         ("hospital", 947),
         ("htmlconf", 1848)
                ]

    dataset = datasets[0]

    g = load_graph(dataset_path + dataset[0] + ".txt")
    print(g.number_of_nodes(), g.number_of_edges(), g.number_of_edges()/g.number_of_nodes())

    run(g, start=dataset[1], dsname=dataset[0])
