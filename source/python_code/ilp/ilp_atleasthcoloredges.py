import networkx as nx
from gurobipy import GRB, Model, tupledict
import gurobipy as gp

from utils import dataset_path, readFile

TIMELIMIT = 60 * 60 * 36
LOGFILE = 'gurobilog.txt'

def run_ilp(nodes, edges, colors_at_edges, edges_by_color, h, k):
    m = Model("ecdspilp")

    m.setParam('TimeLimit', TIMELIMIT)
    m.setParam('LogFile', LOGFILE)

    x = tupledict()
    y = tupledict()

    for u in nodes:
        y[u] = m.addVar(vtype=GRB.BINARY, name='y[%d]' % u)

    for u, v in edges:
        x[u, v] = m.addVar(vtype=GRB.BINARY, name='x[%d,%d]' % (u, v))

        # m.addConstr(0 <= x[u, v])
        m.addConstr(x[u, v] <= y[u])
        m.addConstr(x[u, v] <= y[v])

    for c, r in h.items():
        m.addConstr(gp.quicksum(x[e[0], e[1]] for e in edges_by_color[c]) >= r)

    m.addConstr(gp.quicksum(y[u] for u in nodes) == k)

    m.setObjective((gp.quicksum(x[u, v]*len(colors_at_edges[(u,v)]) for u, v in edges)) / k, GRB.MAXIMIZE)

    m._vars = vars
    m.optimize()

    if m.status != GRB.OPTIMAL:
        print("infeasible")
        return {}

    m.printStats()

    print("result:")
    vals = m.getAttr('x', x)

    return vals

def run_exact(G, k, h):
    colors_at_edges = {}
    edges = set()
    nodes = set()
    edges_by_color = {}
    for u, v, f, d in G.edges(data=True, keys=True):
        edges.add((u, v))
        nodes.add(u)
        nodes.add(v)
        if not (u, v) in colors_at_edges:
            colors_at_edges[(u, v)] = []
        colors_at_edges[(u, v)].append(d["color"])

        if d['color'] not in edges_by_color:
            edges_by_color[d['color']] = []
        edges_by_color[d['color']].append((u, v))

    result = run_ilp(nodes, edges, colors_at_edges, edges_by_color, k=k, h=h)

    num_edges = 0
    nodes = set()
    for r in result:
        assignment = result[r]
        if assignment == 1.0:
            u = r[0]
            v = r[1]
            nodes.add(u)
            nodes.add(v)
            num_edges += len(colors_at_edges[(u, v)])

    return num_edges / k


def load_graph(filename):
    data = readFile(filename)
    g = nx.MultiGraph()
    colors = {}
    for d in data:
        s = d.split(" ")
        u = int(s[0])
        v = int(s[1])
        c = int(s[2])
        g.add_edge(u, v, color=c)
        if c not in colors: colors[c] = 0
        colors[c] += 1
    print(colors)
    return g


def run(dataset):
    g = load_graph(dataset_path + dataset + ".txt")
    print(g.number_of_nodes(), g.number_of_edges(), g.number_of_edges() / g.number_of_nodes())
    approx_data = readFile(dataset_path + dataset + ".txt_rndselection.txt")
    approx = []

    for d in approx_data[2:]:
        s = d.split(" ")
        h = {}
        for i in range(0, len(s)-2):
            h[i] = int(s[i])
        approx.append((h, float(s[-1])))

    print(approx)

    total_result = []

    with open(dataset + "_ilp_color_results.txt", "w") as output:
        for a in approx:
            h = a[0]
            lower = 1
            upper = g.number_of_nodes()

            results = {}

            for k in range(lower, upper):
                r = run_exact(g.copy(), k, h)
                results[k] = r

            res = max(results.items(), key=lambda k: k[1])
            total_result.append((a[1], res[1]))

            output.write(str(a[1]) + " " + str(res) + "\n")
            output.flush()


if __name__ == '__main__':
    run("aucs")
    run("hospital")
    run("htmlconf")
