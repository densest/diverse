import pandas as pd

from utils import dataset_path

def read_file(datasetname):
    df = pd.read_csv(dataset_path + datasetname + ".txt_at_leasth_rt.txt")
    df = df.drop('Unnamed: 9', axis=1)
    means = df.mean()
    std_devs = df.std()
    s = ""
    for r in range(len(means)):
        s = s + " & " + "{:.2f}".format(means[r]) + "\spm{" + "{:.1f}".format(std_devs[r]) + "}"
    print(s)
    return df

if __name__ == '__main__':
    datasets = ["aucs", "airports", "rattus", "knowledge", "epinions", "twitter",
                "hospital", "htmlconf", "fftwyt", "friendfeed", "homo", "dblp" ]

    for ds in datasets:
        df = read_file(ds)

