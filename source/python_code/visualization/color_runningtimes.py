import pandas as pd
from utils import readFile, dataset_path

datasets = [
    "aucs", "hospital", "htmlconf",
    "dkpol", "rattus", "knowledge",
    "epinions", "dblp", "twitter",
    "fftwyt", "homo", "friendfeed"
]


def print_table(df):
    dfm = df.groupby(['dataset', 'r', 'Alg.'], as_index=False)['value'].mean()
    dfstd = df.groupby(['dataset', 'r', 'Alg.'], as_index=False)['value'].std()
    dfm.rename(columns={'value': 'mean'}, inplace=True)
    dfstd.rename(columns={'value': 'std'}, inplace=True)
    df = pd.concat([dfm, dfstd['std']], axis=1)

    rs = [2, 4, 6, 8]
    s = ""
    for r in rs:
        sub = df[df['r'] == r]
        print(sub)
        heuristic_m = sub[sub['Alg.'] == "Heuristic"]["mean"].iloc[0]
        colapprox_m = sub[sub['Alg.'] == "ColApprox"]["mean"].iloc[0]
        heuristic_s = sub[sub['Alg.'] == "Heuristic"]["std"].iloc[0]
        colapprox_s = sub[sub['Alg.'] == "ColApprox"]["std"].iloc[0]
        s = s + " & {:.2f}".format(heuristic_m) + "\spm{" + "{:.1f}".format(heuristic_s) + "} & " + "{:.2f}".format(
            colapprox_m) + "\spm{" + "{:.1f}".format(colapprox_s) + "}"

    print(s)


def getDataFrame():
    results = {'dataset': [], 'r': [], 'Alg.': [], 'value': []}

    for datasetname in datasets:
        path = dataset_path + datasetname + ".txt_colorrunningtimes.txt"
        data = readFile(path)
        for d in data[1:]:
            s = d.split(" ")
            r = 1
            for i in range(0, len(s), 2):
                results['dataset'].append(datasetname)
                results['r'].append(r)
                results['value'].append(float(s[i]))
                results['Alg.'].append("ColApprox")

                results['dataset'].append(datasetname)
                results['r'].append(r)
                results['value'].append(float(s[i + 1]))
                results['Alg.'].append("Heuristic")
                r += 1

    df = pd.DataFrame(results)
    print(df)
    return df


if __name__ == '__main__':
    df = getDataFrame()
    print_table(df)
