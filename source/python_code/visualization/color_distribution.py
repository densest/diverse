import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt

colors_knowledge = [0.0780618, 0.0336966, 0.218435, 0.0290352, 0.236231, 0.0483979, 0.0187872, 0.0487609, 0.150258, 0.0218948, 0.0184685, 0.00562643, 0.0142985, 0.0159895, 0.0118239, 0.0130678, 0.00301463, 0.00208944, 0.00253654, 0.00585663, 0.00192122, 0.00381145, 0.00862336, 0.00103144, 0.00427627, 0.00148297, 0.000451531, 0.00126163, 0.00053564, 0.00027446,]
colors_knowledge_dsp = [0, 7.88208e-05, 0.000236463, 0.0117443, 0.628754, 0.0231733, 0.0135572, 0, 0, 0.239773, 0, 0.0452432, 0, 0.00370458, 0.00449279, 0.0071727, 0, 0, 0, 0.000315283, 0, 0.0198629, 0, 0, 0, 0.0018917, 0, 0, 0, 0,]

colors_binary2 = [
    {'name':'Epinion (Graph)', "v":0.169939, 'Color':0, 't':'Graph'},
    {'name':'Epinion (Graph)', "v":0.830061, 'Color':1, 't':'Graph'},
    {'name': 'Epinion (DSP)', "v": 0.00935866, 'Color': 0, 't': 'DSP'},
    {'name': 'Epinion (DSP)', "v": 0.990641, 'Color': 1, 't': 'DSP'},
    {'name':'Twitter (Graph)', "v":0.255278, 'Color':0, 't':'Graph'},
    {'name':'Twitter (Graph)', "v":0.744722, 'Color':1, 't':'Graph'},
    {'name': 'Twitter (DSP)', "v": 0.0588987, 'Color': 0, 't': 'DSP'},
    {'name': 'Twitter (DSP)', "v": 0.941101, 'Color': 1, 't': 'DSP'},
]

def plot_multiple_colors(colorhist, name):
    sns.set(rc={'figure.figsize': (4, 3)})
    sns.set_style("whitegrid")
    ax = sns.barplot(x=np.arange(len(colorhist)), y=np.array(colorhist))
    i = 0
    for label in ax.xaxis.get_ticklabels():
        if i%4 != 0:
            label.set_visible(False)
        i+=1

    plt.xlabel("Color")
    plt.ylabel("Ratio")
    plt.tight_layout()
    plt.savefig('pdfs/' + name)
    plt.show()



def plot_all_binary_color2(colors, name):
    df = pd.DataFrame(colors)
    sns.set(rc={'figure.figsize': (4.5, 3.5)})
    sns.set_style("whitegrid")
    ax = sns.barplot(data=df, x='name', y='v', hue='Color')
    plt.xticks(rotation=40, ha='right')
    plt.xlabel("")
    plt.ylabel("Ratio")
    legend = plt.legend(loc='upper left', bbox_to_anchor=(0.7, 1))
    legend.set_title("Color")

    plt.tight_layout()
    plt.savefig('pdfs/' + name)
    plt.show()



if __name__ == '__main__':
    plot_multiple_colors(colors_knowledge, "knowledge_all.pdf")
    plot_multiple_colors(colors_knowledge_dsp, "knowledge_dsp.pdf")

    plot_all_binary_color2(colors_binary2, "binary_datasets_all2.pdf")
