import statistics

import matplotlib.pyplot as plt
import seaborn as sns

from utils import readFile, dataset_path


def get_statistics(file, approx1):
    data = readFile(file)

    approx = []
    ilp = []
    for d in data:
        s = d.split(" ")
        a = float(s[0])
        e = float(s[-1].split(")")[0])
        approx.append(a)
        ilp.append(e)

    errors = []
    zero_err = 0
    non_zero_err = 0
    in_one_percent = 0

    for i in range(0, len(ilp)):
        x = round(ilp[i], 6)
        y = approx1[i]
        if y > x:
            print(y, x)

        err = abs(1 - y/x)

        if err != 0:
            non_zero_err += 1
            errors.append(err*100)
            if err*100 <= 1:
                in_one_percent += 1

        else:
            zero_err += 1
            in_one_percent += 1


    opt_fraction = zero_err / (len(ilp))

    print(opt_fraction, zero_err, non_zero_err, in_one_percent, statistics.mean(errors), statistics.stdev(errors),
        statistics.median(errors), max(errors))

def plot_dia(file, file2, title):
    data = readFile(file2)

    ilp = []
    for d in data:
        s = d.split(" ")
        e = float(s[-1].split(")")[0])
        ilp.append(e)


    data = readFile(file)
    s = data[0].split(',')
    gc = []
    for i in range(1, len(s), 2):
        x = int(s[i].split(")")[0])
        gc.append(x)



    s = data[1].split(',')
    lb = []
    for i in range(1, len(s), 2):
        x = int(s[i].split(")")[0])
        lb.append(x)

    g = sum(gc)-sum(lb)

    r1 = []
    r2 = []
    r3 = []
    for d in data[2:]:
        s = d.split(" ")
        si = [int(x) for x in s[:-2]]
        for i in range(len(si)):
            si[i] = si[i]-lb[i]
        y = 0
        for i in range(len(si)):
            y += si[i]/g
        r1.append(y)
        r2.append(float(s[-2]))
        r3.append(float(s[-1]))


    sns.set(rc={'figure.figsize': (4, 3)})
    sns.set_style("whitegrid")
    ax = sns.scatterplot(x=r1, y=r3, marker="d", alpha=0.99, label="Heuristic")
    ax = sns.scatterplot(x=r1, y=r2, marker="o", alpha=0.99, label="ColApprox")
    sns.scatterplot(x=r1, y=ilp, ax=ax, marker="X", alpha=0.7, label="ColILP")
    ax.set(xlabel='$\lambda$', ylabel="Density")
    plt.tight_layout()
    plt.savefig(title + '_scatter.pdf')
    plt.show()

    plt.show()

    return r2, r3

def run(dataset):
    r2, r3 = plot_dia(dataset_path + dataset + ".txt_rndselection.txt",
                      "../ilp/" + dataset + "_ilp_color_results.txt", dataset)
    print("stats approx", dataset)
    get_statistics("../ilp/" + dataset + "_ilp_color_results.txt", r2)
    print("stats heuristic", dataset)
    get_statistics("../ilp/" + dataset + "_ilp_color_results.txt", r3)


if __name__ == '__main__':
    run("aucs")
    run("hospital")
    run("htmlconf")

