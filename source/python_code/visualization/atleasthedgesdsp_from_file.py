import seaborn as sns
from matplotlib import pyplot as plt

from utils import readFile, dataset_path


def plot(values, sizes, title, ylabel, log=False):
    sns.set(rc={'figure.figsize': (4, 3)})
    sns.set_style("whitegrid")
    ax = sns.lineplot(x=sizes, y=values, linewidth=2)
    ax.set(xlabel='$i$', ylabel=ylabel)
    if log:
        ax.set_xscale('log')

    plt.tight_layout()
    plt.savefig(title + '_atleasth.pdf')
    plt.show()


def read_file(datasetname):
    data = readFile(dataset_path + datasetname + ".txt_at_leasth_exp.txt")
    densities = []
    sizes = []
    times =[]
    s = data[0].split(",")
    for i in s[:-1]:
        densities.append(float(i))
    s = data[1].split(",")
    for i in s[:-1]:
        sizes.append(float(i))
    s = data[2].split(",")
    for i in s[:-1]:
        times.append(float(i))
    return densities, sizes, times


if __name__ == '__main__':
    datasets = ["aucs", "airports", "rattus", "knowledge", "epinions", "twitter",
                "hospital", "htmlconf", "fftwyt", "friendfeed", "homo", "dblp" ]

    for ds in datasets:
        densities, sizes, times = read_file(ds)
        plot(values=densities, sizes=sizes, title=ds, ylabel="Density $d(S_i)$", log=True)

