#include <iostream>
#include <fstream>
#include <cmath>
#include "util/Params.h"
#include "graph/graph.h"
#include "algorithms/unconstrainedDSPApprox.h"
#include "algorithms/atLeasthEdgesDSPApprox.h"
#include "algorithms/atLeasthColoredEdgesDSPApprox.h"
#include "util/IO.h"
#include "algorithms/baseline.h"

#define MAX_COLOR 1000000

using namespace std;


bool verify(std::map<Color, uint> const &reqs, std::map<Color, uint> const& colors){
    for (auto &p :reqs) {
        if (p.second == 0) continue;
        if (!colors.contains(p.first)) {
            cout << "wrong: " << p.first << " " << p.second << " not in colors " << endl;
            return false;
        }
        if (colors.at(p.first) < p.second) {
            cout << "wrong: " << p.first << " " << p.second << " " << colors.at(p.first) << endl;
            return false;
        }
    }
    return true;
}


int main(int argc, char *argv[]) {
    srand(42);
//    srand(time(nullptr));
    vector<string> args;
    for (int i = 1; i < argc; ++i) args.emplace_back(argv[i]);

    Params params;
    if (!params.parseArgs(args)) {
        show_help();
        return 1;
    }

    Timer timer;

    bool no_cols = params.task == ComputeAtLeastxEdgesDSPrunningtime
            || params.task == ComputeAtLeastxEdgesDSPmultiple || params.task == ComputeAtLeastxEdgesDSP;

    graph g = load_graph(params.dataset_path, no_cols);

    if (params.m) {
        auto e = edge{g.max_nid+1, g.max_nid+2, MAX_COLOR};
        g.edges.push_back(e);
        g.max_nid += 2;

        g.adjacent.emplace_back();
        g.adjacent.emplace_back();
        g.neighbors.emplace_back();
        g.neighbors.emplace_back();

        g.adjacent[e.u].push_back(e);
        g.adjacent[e.v].push_back(e);
        g.neighbors[e.u].insert(e.v);
        g.neighbors[e.v].insert(e.u);
        g.colors[e.c]++;
    }

    switch (params.task) {

        case ComputeAtLeastxEdgesDSP: {
            computeAtLeasthEdgesDSPApproximation(g, params.x);
            break;
        }
        case ComputeAtLeastxEdgesDSPmultiple: {
            if (params.x == 1) {

                auto dsp_size = computeUnconstrainedDSPApproximation(g);
                auto size = dsp_size.second + 1;
                auto step = 0;
                auto step_size = 1;
                vector<double> densities;
                vector<double> times;
                vector<double> sizes;
                while (true) {
                    auto next = size + step;
                    sizes.push_back(step);
                    cout << "\tsize: " << size << "\tstep_size: " << step_size << "\tnext: " << next << "\tof: " << g.edges.size() << endl;

                    timer.start();
                    auto r = computeAtLeasthEdgesDSPApproximation(g, next);//+ step
                    times.push_back(timer.stop());
                    cout << "Elapsed: " << times.back() << endl;

                    densities.push_back((double) r.second.size() / (double) r.first.size());

                    if (size + step > g.edges.size()) break;
                    step += step_size;

                }

                print_vector(densities);
                break;
            }



            auto dsp_size = computeUnconstrainedDSPApproximation(g);
            auto size = dsp_size.second + 1;
            auto step = 0; //(g.edges.size() - size) / 10;
            auto step_size = max((int)0, (int)((g.edges.size()-size)/params.x))+1;
            vector<double> densities;
            vector<double> times;
            vector<double> sizes;
            while (true) {
                auto next = size + step;
                sizes.push_back(step);
                cout << "\tsize: " << size << "\tstep_size: " << step_size << "\tnext: " << next << "\tof: " << g.edges.size() << endl;

                timer.start();
                auto r = computeAtLeasthEdgesDSPApproximation(g, next);//+ step
                times.push_back(timer.stop());
                cout << "Elapsed: " << times.back() << endl;

                densities.push_back((double) r.second.size() / (double) r.first.size());

                if (size + step > g.edges.size()) break;
                step += step_size;

            }

            print_vector(densities);
            print_vector(sizes);
            print_vector(times);
            write_vectors_to_file_lines(vector<vector<double>>{densities, sizes, times},
                                        params.dataset_path + "_at_leasth_exp.txt");
            break;
        }
        case ComputeAtLeastxEdgesDSPrunningtime: {

            auto dsp_size = computeUnconstrainedDSPApproximation(g);
            auto size = dsp_size.second;
            auto rest = g.edges.size() - size;

            auto percentage = vector<double>{0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9};

            vector<vector<double>> times;

            for (int i = 0; i <= params.x; ++i) {
                times.emplace_back();
                for (auto p: percentage) {
                    auto step = size + (int) (p * (double) rest);
                    auto r = computeAtLeasthEdgesDSPApproximation_rt(g, step);
                    times.back().push_back(r);
                    cout << "\tsize: " << step << "\tof: " << g.edges.size() << " " << r << endl;
                }
            }
            times[0] = percentage;

            write_vectors_to_file_lines(times, params.dataset_path + "_at_leasth_rt.txt");
            break;
        }

        case ComputeAtLeastxColoredEdgesDSP: {
            uint total_color_requirement = 0;
            if (params.x >= 1) {
                for (auto &p : g.colors) {
                    params.color_requirements[p.first] = g.colors[p.first] / params.x;
                }
            }

            for (auto &p: params.color_requirements) {
                if (p.second > g.colors[p.first]) {
                    cout << p.second << " " << g.colors[p.first] << " " << p.first << endl;
                    cout << "No feasible solution!" << endl;
                    exit(0);
                }
                total_color_requirement += p.second;
            }
            auto result = computeAtLeasthColoredEdgesDSPApproximation(g, params.color_requirements,
                                                                      total_color_requirement);

            cout << "[";
            for (auto &n : result.nids) {
                cout << n << ", ";
            }
            cout << "]" << endl;

            break;
        }

        case ComputeAtLeastxColoredEdgesDSPAll_rndselection: {

            auto sg = computeUnconstrainedDSPApproximationAndGetGraph(g);

            std::map<Color, uint> sg_colors;
            for (auto &c : g.colors) sg_colors[c.first] = 0;
            for (auto &e: sg.second) {
                sg_colors[e.c]++;
            }
            std::map<Color, uint> cdivs;
            std::vector<uint> cdivsvec(g.colors.size(), 0);

            for (auto &p: g.colors) {
                cdivs[p.first] = p.second - sg_colors[p.first];
                cdivsvec[p.first] = (p.second - sg_colors[p.first]);
            }


            vector<std::map<Color, uint>> all_reqs;
            ofstream outfile(params.dataset_path + "_rndselection.txt");

            for (auto &c : g.colors) {
                outfile << "(" << c.first << "," << c.second << "),";
            }
            outfile << endl;
            for (auto &c : sg_colors) {
                outfile << "(" << c.first << "," << c.second << "),";
            }
            outfile << endl;

            for (int i = 0; i < params.x; ++i) {
                std::map<Color, uint> reqs = sg_colors;
                for (Color p = 0; p < (Color)cdivsvec.size(); ++p) {
                    if (cdivsvec[p] > 0)
                        reqs[p] += (rand() % (cdivsvec[p]));
                }

                auto total_color_requirement = 0;
                for (auto &r : reqs) {
                    total_color_requirement += r.second;
                }

                auto result1 = computeAtLeasthColoredEdgesDSPApproximation(g, reqs, total_color_requirement);
                if (!verify(reqs, result1.colors)) {exit(1);}
                auto result2 = computeAtLeasthColoredEdgesDSPBaselineHeuristic(g, reqs);
                if (!verify(reqs, result2.colors)) {exit(1);}

                for (auto &p: reqs) {
                    outfile << p.second << " ";
                }
                outfile << to_string(result1.density)  << " " << to_string(result2.density) << endl;
            }

            outfile.close();
            for (auto &p: cdivs) {
                cout << p.first << " " << p.second << " " << sg_colors[p.first] << endl;
            }

            break;
        }
        case ComputeAtLeastxColoredEdgesDSPAll_colorfractions: {
            auto sg = computeUnconstrainedDSPApproximationAndGetGraph(g);

            std::map<Color, uint> sg_colors;
            for (auto &e: sg.second) {
                sg_colors[e.c]++;
            }
            std::map<Color, uint> cdivs;

            for (auto &p: g.colors) {
                if (sg_colors.find(p.first) != sg_colors.end()) {
                    cdivs[p.first] = round((p.second - sg_colors[p.first]) / (double)params.x);
                } else {
                    cdivs[p.first] = round((p.second) / (double)params.x);
                }
            }

            vector<std::map<Color, uint>> all_reqs;
            ofstream outfile(params.dataset_path + "_colorfractions_m" + to_string(params.m) + ".txt");

            for (int i = 1; i <= params.x; ++i) {
                std::map<Color, uint> reqs;
                for (auto c : g.colors) reqs[c.first] = 0;
                for (auto &p : cdivs) {
                    reqs[p.first] += min(g.colors[p.first], sg_colors[p.first] + i*cdivs[p.first]);
                }
                if (params.m) {
                    reqs[MAX_COLOR] = 1;
                }

                auto total_color_requirement = 0;
                for (auto &r : reqs) {
                    total_color_requirement += r.second;
                }

                auto result1 = computeAtLeasthColoredEdgesDSPApproximation(g, reqs, total_color_requirement);
                if (!verify(reqs, result1.colors)) {exit(1);}

                auto result3 = computeAtLeasthColoredEdgesDSPBaselineHeuristic(g, reqs);
                if (!verify(reqs, result3.colors)) {
                    exit(1);
                }

                auto result2 = result1;


                outfile << get_colors(reqs)
                << "," << to_string(result1.density) << " " << to_string(result1.rt)
                << " " << to_string(result1.nodes) << " " << to_string(result1.edges) << "," << get_colors(result1.colors)
                << "," << to_string(result2.density) << " " << to_string(result2.rt)
                << " " << to_string(result2.nodes) << " " << to_string(result2.edges) << "," << get_colors(result2.colors)
                << "," << to_string(result3.density) << " " << to_string(result3.rt)
                << " " << to_string(result3.nodes) << " " << to_string(result3.edges) << "," << get_colors(result3.colors)
                << endl;
            }

            outfile.close();
            for (auto &p: cdivs) {
                cout << p.first << " " << p.second << " " << sg_colors[p.first] << endl;
            }

            break;
        }

        case ComputeAtLeastxColoredEdgesDSPAll_runningtimes: {
            auto sg = computeUnconstrainedDSPApproximationAndGetGraph(g);

            std::map<Color, uint> sg_colors;
            for (auto &e: sg.second) {
                sg_colors[e.c]++;
            }
            std::map<Color, uint> cdivs;
            std::vector<uint> cdivsvec;

            for (auto &p: g.colors) {
                if (sg_colors.find(p.first) != sg_colors.end()) {
                    cdivs[p.first] = round((p.second - sg_colors[p.first]) / (double)params.x);
                } else {
                    cdivs[p.first] = round((p.second) / (double)params.x);
                }
                cout << cdivs[p.first] << endl;
            }

            vector<std::map<Color, uint>> all_reqs;
            ofstream outfile(params.dataset_path + "_colorrunningtimes.txt");

            outfile << "T" << endl;

            for (int j = 0; j < 10; ++j) {
                for (int i = 1; i <= params.x; ++i) {

                    std::map<Color, uint> reqs;
                    for (auto c: g.colors) reqs[c.first] = 0;
                    for (auto &p: cdivs) {
                        reqs[p.first] += min(g.colors[p.first], sg_colors[p.first] + i * cdivs[p.first]);
                    }

                    auto total_color_requirement = 0;
                    for (auto &r: reqs) {
                        total_color_requirement += r.second;
                    }

                    auto result1 = computeAtLeasthColoredEdgesDSPApproximation(g, reqs,
                                                                               total_color_requirement);

                    auto result2 = computeAtLeasthColoredEdgesDSPBaselineHeuristic(g, reqs);


                    outfile << to_string(result1.rt) << " " << to_string(result2.rt) << " ";
                }
                outfile << endl;
            }
            outfile.close();

            break;
        }

        case ComputeAtLeastxColoredEdgesDSPBaseline:{
            for (auto &p: params.color_requirements) {
                if (p.second > g.colors[p.first]) {
                    cout << p.second << " " << g.colors[p.first] << " " << p.first << endl;
                    cout << "No feasible solution!" << endl;
                    exit(0);
                }
            }

            computeAtLeasthColoredEdgesDSPBaselineHeuristic(g, params.color_requirements);

            break;
        }

        case GraphInfo: {
            cout << "#colors: " << g.colors.size() << endl;

            map<pair<NodeId, NodeId>, set<Color>> colsatnodes;
            uint max_cols_at_edges = 0;
            uint min_cols_at_edges = g.edges.size();
            set<pair<NodeId, NodeId>> flat_edges;
            set<NodeId> flat_nodes;
            for (auto &e : g.edges) {
                if (!colsatnodes.contains({e.u, e.v}))colsatnodes[{e.u, e.v}] = set<Color>();
                colsatnodes[{e.u, e.v}].insert(e.c);
                flat_edges.insert({e.u, e.v});
                flat_nodes.insert(e.u);
                flat_nodes.insert(e.v);
            }
            for (auto &e : g.edges) {
                if (colsatnodes[{e.u, e.v}].size() > max_cols_at_edges) max_cols_at_edges = colsatnodes[{e.u, e.v}].size();
                if (colsatnodes[{e.u, e.v}].size() < min_cols_at_edges) min_cols_at_edges = colsatnodes[{e.u, e.v}].size();
            }
            cout << "min # colors at edge: " << min_cols_at_edges << endl;
            cout << "max # colors at edge: " << max_cols_at_edges << endl;
            cout << "#nodes flat: " << flat_nodes.size() << endl;
            cout << "#edges flat: " << flat_edges.size() << endl;
            break;
        }

        default: {
            show_help();
        }
    }

    return 0;
}
