#ifndef FAIREDSP_BASELINE_H
#define FAIREDSP_BASELINE_H

#include "../graph/graph.h"
#include "atLeasthColoredEdgesDSPApprox.h"


Result computeAtLeasthColoredEdgesDSPBaselineHeuristic(graph const &graph,
                                                       const std::map<Color, uint>& min_color_requirements);

#endif //FAIREDSP_BASELINE_H
