#include <set>
#include <map>
#include <iostream>
#include "atLeasthColoredEdgesDSPApprox.h"
#include "../util/IO.h"

using namespace std;


AtLeasthEdgesResult computeAtLeasthEdgesDSPApproximationForColorApprox(graph const &graph,
                                                                       std::map<Color, uint> min_color_requirements, uint h) {
    Timer timer;
    timer.start();

    std::map<uint, std::vector<edge>> g;

    NodeId mxnid = graph.max_nid;
    for (auto &e : graph.edges) {
        g[e.u].push_back(e);
        g[e.v].push_back({e.v, e.u, e.c});
    }


    std::set<std::pair<uint, NodeId>, std::less<>> degrees;
    std::vector<uint> c(mxnid + 1, 0);
    for (auto &p : g) {
        degrees.insert({p.second.size(), p.first});
        c[p.first] = p.second.size();
    }

    uint nodes = graph.max_nid + 1;
    uint edges = graph.edges.size();
    double max_density = 0;

    vector<bool> removed_nids(mxnid + 1, false);
    vector<bool> best_removed(mxnid + 1, false);

    auto density = (double)edges/(double)nodes;
    if (density > max_density) {
        max_density = density;
    }

    std::vector<bool> back_insertion(mxnid + 1, false);
    std::vector<bool> max_back_insertion(mxnid + 1, false);

    auto colors_left = graph.colors;

    while (!degrees.empty()) {

        auto next = degrees.begin();
        auto u = next->second;
        degrees.erase(next);

        if (removed_nids[u]) continue;
        removed_nids[u] = true;
        removed_nids[u] = true;


        nodes--;
        bool add_to_back_insertion = false;
        set<NodeId> to_add;

        for (auto &e : g[u]) {
            if (removed_nids[e.v]) continue;
            if (c[e.v] > c[e.u]) {
                --c[e.v];
                degrees.insert({c[e.v], e.v});
            }
            if (colors_left[e.c] > 0) {
                colors_left[e.c]--;
            }
            if (colors_left[e.c] < min_color_requirements[e.c]) {
                add_to_back_insertion = true;
                back_insertion[e.v] = true;
                to_add.insert(e.v);
            }
            edges--;
        }
        if (add_to_back_insertion) {
            back_insertion[u] = true;
        }

        if (edges < h) break;

        density = (double)edges/(double)nodes;
        if (density > max_density) {
            max_density = density;
            best_removed = removed_nids;
            max_back_insertion = back_insertion;
        }

    }

    set<NodeId> remaining_nids;
    for (NodeId i = 0; i <= graph.max_nid; ++i) {
        if (!best_removed[i] || max_back_insertion[i])
            remaining_nids.insert(i);
    }

    auto rt = timer.stop();

    set<NodeId> removed_set;
    for (NodeId i = 0; i <= graph.max_nid; ++i) {
        if (removed_nids[i])
            removed_set.insert(i);
    }

    AtLeasthEdgesResult result;

    vector<edge> remaining_edges;
    vector<edge> removed_edges;
    map<Color, uint> colors;
    for (auto &e : graph.edges) {
        if (remaining_nids.find(e.u) != remaining_nids.end() && remaining_nids.find(e.v) != remaining_nids.end()) {
            remaining_edges.push_back(e);
            result.edges_of_color_in_subgraph[e.c]++;
            colors[e.c]++;
        } else {
            removed_edges.push_back(e);
        }
    }

    result.subgraph = {remaining_nids, remaining_edges};
    result.non_subgraph = {removed_set, removed_edges};
    result.rt = rt;

    return result;
}



Result computeAtLeasthColoredEdgesDSPApproximation(graph const &graph,
                                                   const std::map<Color, uint>& min_color_requirements,
                                                   uint total_min_edges) {

    auto alhr = computeAtLeasthEdgesDSPApproximationForColorApprox(graph, min_color_requirements, total_min_edges);

    cout << "----Solution of at least h colored edges (variant 1)----" << endl;

    // output final graph stats
    auto &result_nids = alhr.subgraph.first;
    vector<edge> &result_edges = alhr.subgraph.second;

    double density = (double)result_edges.size()/(double)result_nids.size();
    auto rt = alhr.rt;

    map<Color, uint> final_colors;
    for (auto &p : graph.colors) final_colors[p.first] = 0;
    for (auto &e : result_edges) final_colors[e.c]++;

    cout << "Elapsed: " << rt << endl;
    cout << "nodes: " << result_nids.size() << endl;
    cout << "edges: " << result_edges.size() << endl;
    cout << "density: " << density << endl;
    cout << "colors:" << endl;
    for (auto &p : final_colors) {
        if (min_color_requirements.find(p.first) != min_color_requirements.end())
            cout << p.first << "\t" << p.second << "\t" << (double)p.second/(double)result_edges.size() << " (" << min_color_requirements.at(p.first) << " req.)" << endl;
        else
            cout << p.first << "\t" << p.second << "\t" << (double)p.second/(double)result_edges.size() << endl;
    }

    return Result{density, rt, result_nids.size(), result_edges.size(), final_colors, result_nids};
}
