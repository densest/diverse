#ifndef FAIREDSP_IO_H
#define FAIREDSP_IO_H

#include <sstream>
#include <iomanip>
#include <fstream>
#include <chrono>

template<typename T>
void write_vectors_to_file_lines(const std::vector<std::vector<T>>& vecs, const std::string &filename) {
    std::ofstream fs;
    fs.open(filename);
    if (!fs.is_open()) {
        std::cout << "Could not write data to " << filename << std::endl;
        return;
    }
    for (auto &data : vecs) {
        for (auto &d: data)
            fs << d << ",";
        fs << std::endl;
    }
    fs.close();
}

template<typename T>
std::vector<T> split_string(const std::string& s, char delim) {
    std::vector<T> result;
    std::vector<std::string> strings;
    std::stringstream ss(s);
    while (ss.good()) {
        std::string substr;
        getline(ss, substr, delim);
        if (!substr.empty()) {
            try {
                stol(substr);
            }
            catch(...) {
                continue;
            }
            result.push_back(stol(substr));
        }
    }
    return result;
}

template<typename T>
std::string get_colors(std::map<Color, T> col) {
    std::string s = "(";
    for (auto &p : col) {
        s += std::to_string(p.first) + "-"+std::to_string(p.second)+" ";
    }
    s += ")";
    return s;
}

template<typename T>
void print_vector(std::vector<T> const &data) {
    std::cout << std::setprecision(16) << std::endl;
    std::cout << "[";
    for (auto d: data) {
        std::cout << d << ", ";
    }
    std::cout << "]" << std::endl;
}

class Timer {
public:
    void start() {
        timePoint = std::chrono::high_resolution_clock::now();
    }

    double stop() {
        auto finish = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> elapsed = finish - timePoint;
        return elapsed.count();
    }

    double stopAndPrintTime() {
        auto t = stop();
        std::cout << "Elapsed: " << t << std::endl;
        return t;
    }

private:
    std::chrono::high_resolution_clock::time_point timePoint{};
};


#endif //FAIREDSP_IO_H
