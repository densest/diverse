#ifndef PARAMS_H
#define PARAMS_H

#include <string>
#include <vector>
#include <map>
#include "../graph/graph.h"

void show_help();

enum Task {
    ComputeAtLeastxEdgesDSP,
    ComputeAtLeastxEdgesDSPmultiple,
    ComputeAtLeastxEdgesDSPrunningtime,
    ComputeAtLeastxColoredEdgesDSP,
    ComputeAtLeastxColoredEdgesDSPAll_rndselection,
    ComputeAtLeastxColoredEdgesDSPAll_colorfractions,
    ComputeAtLeastxColoredEdgesDSPAll_runningtimes,
    ComputeAtLeastxColoredEdgesDSPBaseline,
    GraphInfo
};

struct Params {
    std::string dataset_path;
    uint rndseed = 1;
    Task task;
    int x = 1;
    uint m = 0;
    std::map<Color, uint> color_requirements;
    bool parseArgs(std::vector<std::string> args);
};

#endif //PARAMS_H
