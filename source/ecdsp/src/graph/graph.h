#ifndef FAIREDSP_GRAPH_H
#define FAIREDSP_GRAPH_H

#include <cstdlib>
#include <vector>
#include <string>
#include <tuple>
#include <set>
#include <map>

using NodeId = u_int32_t;
using Color = int;

struct edge {
    NodeId u{}, v{};
    Color c{};

    bool operator< (const edge &r) const {
        return std::tie(u, v, c) < std::tie(r.u, r.v, r.c);
    }
};

struct graph {

    graph()= default;

    std::vector<edge> edges;

    NodeId max_nid = 0;

    std::vector<std::set<NodeId>> neighbors;
    std::vector<std::vector<edge>> adjacent;

    std::map<Color, uint> colors;
};

graph load_graph(std::string const &file, bool no_cols);

#endif //FAIREDSP_GRAPH_H
